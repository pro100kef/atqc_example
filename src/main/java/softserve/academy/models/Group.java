package softserve.academy.models;

public class Group {
    String name;
    String location;
    boolean budgetOwner;
    String direction;
    String startDate;
    String finishDate;
    String teacher;
    String expert;
    String stage;

    public Group (){};


    public Group(String name,
                 String location,
                 boolean budgetOwner,
                 String direction,
                 String startDate,
                 String finishDate,
                 String teacher,
                 String expert,
                 String stage) {
        this.name = name;
        this.location = location;
        this.budgetOwner = budgetOwner;
        this.direction = direction;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.teacher = teacher;
        this.expert = expert;
        this.stage = stage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBudgetOwner() {
        return budgetOwner;
    }

    public void setBudgetOwner(boolean budgetOwner) {
        this.budgetOwner = budgetOwner;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getExpert() {
        return expert;
    }

    public void setExpert(String expert) {
        this.expert = expert;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }
}
