package softserve.academy;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.User;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

public class BaseTestK {
    private String login;
    private String password;

    public BaseTestK(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @BeforeClass(alwaysRun = true)
    public void logIn() {
        setDriver()
                .manage()
                .window()
                .maximize();

        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(new User(login, password));

    }

    @AfterClass(alwaysRun = true)
    @AfterGroups(groups = "with bugs")
    public void logOut() {
        getDriver().navigate().to("http://146.148.17.49/logout");
        getDriver().quit();

    }



}
