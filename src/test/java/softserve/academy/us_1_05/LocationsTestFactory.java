package softserve.academy.us_1_05;

import org.testng.annotations.Factory;

public class LocationsTestFactory {

    @Factory
    public Object[] factoryMethod() {
        return new Object[]{
                new SelectLocationModalWindowTest("sasha", "1234"),
                new SelectLocationModalWindowTest("dmytro", "1234"),
                new SelectLocationModalWindowTest("admin", "1234")
        };
    }
}
